import numpy as np
from scipy.spatial import distance
from sortedcontainers import SortedList

def search_similarities_user(user2movie: dict, usermovie2rating: dict):

    """It searches for similarities between users through their rating history

    Args:
        user2movie (dict): A dict in which each key is a user and the value is a list with rated movies
        usermovie2rating (dict): A dict in which each key is a tuple(user, movie) and the value is the rating

    Returns:
        neighbors (SortedList): It returns a list in which each element is a list containing all cosine similatiries between user i, j.
                                If there are 100 users in the dataset, there will be 100 elements, one for each user, correlating the user with
                                all the rest.
        averages (list): It returns the average rating for each i user
        deviations (list): It returns the deviations from the average rating for each movie rated by the i user
    """
    N = max(user2movie.keys()) + 1
    K = 20 #Maximum number of similarities

    neighbors = []
    averages = []
    deviations = []

    for i in range(N):
        movies_i = user2movie[i]
        movies_i_set = set(movies_i)
        ratings_i = { movie:usermovie2rating[(i, movie)] for movie in movies_i }
        avg_i = np.mean(list(ratings_i.values()))
        dev_i = {movie: (rating-avg_i) for movie, rating in ratings_i.items()}

        # save these for later use
        averages.append(avg_i)
        deviations.append(dev_i)

        sl = SortedList()
        for j in range(N):
            if j != i:
                
                movies_j = user2movie[j]
                movies_j_set = set(movies_j)
                common_movies = (movies_i_set & movies_j_set)

                ratings_j = { movie:usermovie2rating[(j, movie)] for movie in movies_j }
                avg_j = np.mean(list(ratings_j.values()))
                dev_j = {movie: (rating-avg_j) for movie, rating in ratings_j.items()}

                if len(common_movies) > 5:
                    common_movies_dev_i = [dev_i[m] for m in common_movies]
                    common_movies_dev_j = [dev_j[m] for m in common_movies]
                    cos_similarity = 1 - distance.cosine(common_movies_dev_i, common_movies_dev_j)

                    sl.add((-cos_similarity, j))

                    if len(sl) > K:
                        del sl[-1]
            
        neighbors.append(sl)

    return neighbors, averages, deviations

def predict_user(i, m, neighbors, deviations, averages):
    # calculate the weighted sum of deviations
    numerator = 0
    denominator = 0
    for neg_w, j in neighbors[i]:
        # remember, the weight is stored as its negative
        # so the negative of the negative weight is the positive weight
        try:
            numerator += -neg_w * deviations[j][m]
            denominator += abs(neg_w)
        except KeyError:
        # neighbor may not have rated the same movie
        # don't want to do dictionary lookup twice
        # so just throw exception
            pass

    if denominator == 0:
        prediction = averages[i]
    else:
        prediction = numerator / denominator + averages[i]
    prediction = min(5, prediction)
    prediction = max(0.5, prediction) # min rating is 0.5
    
    return prediction
