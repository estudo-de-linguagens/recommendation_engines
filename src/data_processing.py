from collections import Counter
import numpy as np
import pandas as pd
from sklearn.utils import shuffle

def filtering_df(df: pd.DataFrame, first_n_users: int, first_n_movies: int):
    """It selects the most frequents users and movies

    Args:
        df (pd.DataFrame): Dataframe containing movie rating history
        first_n_users (int): The n most frequent users
        first_n_movies (int): The n most frequents movies

    Returns:
        [pd.DataFrame]: The filtered DataFrame
    """

    # Count movies per user and also how many times each movie was rated 
    user_ids = Counter(df['userId'])
    movie_ids = Counter(df['movieId'])

    # Sort user and movies by frequency
    top_n_users = [id for id, counting in user_ids.most_common(first_n_users)]
    top_n_movies = [id for id, counting in movie_ids.most_common(first_n_movies)]

    # Selecting only the most common users and movies
    df_filtered = df[df.userId.isin(top_n_users) & df.movieId.isin(top_n_movies)].copy()

    # Dropping timestamp column (not useful)
    df_filtered = df_filtered.drop('timestamp', axis=1)

    # Creating sequential ids
    new_user_id_map = {}
    new_movie_id_map = {}

    idx = 0

    for user in top_n_users:
        new_user_id_map[user] = idx
        idx += 1

    j = 0

    for movie in top_n_movies:
        new_movie_id_map[movie] =  j
        j += 1

    # Rename IDs to the new created IDs
    df_filtered['userId'] = df_filtered.userId.map(new_user_id_map)
    df_filtered['movieId'] = df_filtered.movieId.map(new_movie_id_map)

    return df_filtered

def dict_structures_ratings(df: pd.DataFrame):
    """It creates new data structures to the collaborative filtering algorithm

    Args:
        df (pd.DataFrame): The filtered dataframe.

    Returns:
        [dict]: It returns the dict structures that will be used in the collaborative filtering algorithm
    """

    N = df.userId.max() + 1 # number of users
    M = df.movieId.max() + 1 # number of movies

    # split into train and test
    df = shuffle(df, random_state=12)
    cutoff = int(0.8*len(df))
    df_train = df.iloc[:cutoff]
    df_test = df.iloc[cutoff:]

    # a dictionary to tell us which users have rated which movies
    user2movie = {}
    # a dicationary to tell us which movies have been rated by which users
    movie2user = {}
    # a dictionary to look up ratings
    usermovie2rating = {}
    # test ratings dictionary
    usermovie2rating_test = {}    

    def update_user2movie_and_movie2user(row):

        count = 0        
        count += 1

        if count % 100000 == 0:
            print("processed: %.3f" % (float(count)/cutoff))

        i = int(row.userId)
        j = int(row.movieId)
        if i not in user2movie:
            user2movie[i] = [j]
        else:
            user2movie[i].append(j)

        if j not in movie2user:
            movie2user[j] = [i]
        else:
            movie2user[j].append(i)

        usermovie2rating[(i,j)] = row.rating    
    
    def update_usermovie2rating_test(row):
        count = 0
        count += 1
        if count % 100000 == 0:
            print("processed: %.3f" % (float(count)/len(df_test)))

        i = int(row.userId)
        j = int(row.movieId)
        usermovie2rating_test[(i,j)] = row.rating

    print("Calling: update_user2movie_and_movie2user")
    df_train.apply(update_user2movie_and_movie2user, axis=1)
    
    print("Calling: update_usermovie2rating_test")
    df_test.apply(update_usermovie2rating_test, axis=1)

    return user2movie, movie2user, usermovie2rating, usermovie2rating_test
