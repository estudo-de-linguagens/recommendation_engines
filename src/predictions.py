import numpy as np

def mse(p, t):    
  p = np.array(p)
  t = np.array(t)

  return np.mean((p - t)**2)