import numpy as np
from scipy.spatial import distance
from sortedcontainers import SortedList

def search_similarities_items(movie2user: dict, usermovie2rating: dict):

    """It searches for similarities between items through their rating history

    Args:
        movie2user (dict): A dict in which each key is a movie and the value is a list with userIds that rated the movie
        usermovie2rating (dict): A dict in which each key is a tuple(user, movie) and the value is the rating

    Returns:
        neighbors (SortedList): It returns a list in which each element is a list containing all cosine similatiries between movie i, j.
                                If there are 100 movies in the dataset, there will be 100 elements, one for each movie, correlating the movie with
                                all the rest.
        averages (list): It returns the average rating for each i movie
        deviations (list): It returns the deviations from the average rating for each user rated for the i movie
    """
    N = max(movie2user.keys()) + 1
    K = 20 #Maximum number of similarities

    neighbors = []
    averages = []
    deviations = []

    for i in range(N):
        users_i = movie2user[i]
        users_i_set = set(users_i)
        ratings_i = { user:usermovie2rating[(user, i)] for user in users_i }
        avg_i = np.mean(list(ratings_i.values()))
        dev_i = {user: (rating-avg_i) for user, rating in ratings_i.items()}

        # save these for later use
        averages.append(avg_i)
        deviations.append(dev_i)

        sl = SortedList()
        for j in range(N):
            if j != i:
                
                users_j = movie2user[j]
                users_j_set = set(users_j)
                common_users = (users_i_set & users_j_set)

                ratings_j = { user:usermovie2rating[(user, j)] for user in users_j }
                avg_j = np.mean(list(ratings_j.values()))
                dev_j = {user: (rating-avg_j) for user, rating in ratings_j.items()}

                if len(common_users) > 5:
                    common_users_dev_i = [dev_i[u] for u in common_users]
                    common_users_dev_j = [dev_j[u] for u in common_users]
                    cos_similarity = 1 - distance.cosine(common_users_dev_i, common_users_dev_j)

                    sl.add((-cos_similarity, j))

                    if len(sl) > K:
                        del sl[-1]
            
        neighbors.append(sl)

    return neighbors, averages, deviations

def predict_item_item(m, u, neighbors, deviations, averages):
    # calculate the weighted sum of deviations
    numerator = 0
    denominator = 0
    for neg_w, j in neighbors[m]:
        # remember, the weight is stored as its negative
        # so the negative of the negative weight is the positive weight
        try:
            numerator += -neg_w * deviations[j][u]
            denominator += abs(neg_w)
        except KeyError:
        # neighbor may not have rated the same movie
        # don't want to do dictionary lookup twice
        # so just throw exception
            pass

    if denominator == 0:
        prediction = averages[m]
    else:
        prediction = numerator / denominator + averages[m]
    prediction = min(5, prediction)
    prediction = max(0.5, prediction) # min rating is 0.5
    
    return prediction