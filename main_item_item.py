import pandas as pd
import os
import time

start_time = time.time()

# Custom functions
from src.data_processing import filtering_df, dict_structures_ratings
from src.collaborative_item_item import search_similarities_items, predict_item_item
from src.predictions import mse

original_path = os.getcwd()

print('##########Reading data##########')
df = pd.read_csv(os.getcwd() + '\\data\\rating.csv')

print('##########Filtering data##########')
# Selecting only the most frequent users and movies
df_filtered = filtering_df(df=df, first_n_users=100, first_n_movies=20)

print('##########Generating dict structures for calculations##########')
# Creating dicts mapping which movies each user rated and also the tuple of user-movie and its respective rating
user2movie, movie2user, usermovie2rating, usermovie2rating_test = dict_structures_ratings(df_filtered)

print('##########Calculating similarity between movies##########')
# Generating similarity calculations
similarities, averages, deviations = search_similarities_items(movie2user, usermovie2rating)

print('##########Generating predictions and accuracy metricss##########')
train_predictions = []
train_targets = []

for (i, m), target in usermovie2rating.items():
  # calculate the prediction for this movie
  prediction = predict_item_item(m, i, neighbors=similarities, deviations=deviations, averages=averages)

  # save the prediction and target
  train_predictions.append(prediction)
  train_targets.append(target)

test_predictions = []
test_targets = []

# same thing for test set
for (i, m), target in usermovie2rating_test.items():
  # calculate the prediction for this movie
  prediction = predict_item_item(m, i, neighbors=similarities, deviations=deviations, averages=averages)

  # save the prediction and target
  test_predictions.append(prediction)
  test_targets.append(target)

print('train mse:', mse(train_predictions, train_targets))
print('test mse:', mse(test_predictions, test_targets))

print("--- %s seconds ---" % (time.time() - start_time))